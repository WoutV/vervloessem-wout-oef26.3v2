﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vervloessem_Wout_oef26._3v2
{
    class Republiek : Land
    {
        //private string _president;
        public string President { get; set; }

        public Republiek(string naam, string hoofdstad, string president)
        {
            Naam = naam;
            Hoofdstad = hoofdstad;
            President = president;
        }
        public override string ToString()
        {
            return base.ToString();
        }
    }
}

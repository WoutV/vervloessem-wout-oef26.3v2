﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Vervloessem_Wout_oef26._3v2
{
    class Land
    {
        //private string _hoofstad;
        //private string _naam;

        public string Hoofdstad { get; set; }
        public string Naam { get; set; }

        public Land()
        {

        }
        public Land(string naam, string hoofdstad)
        {
            Naam = naam;
            Hoofdstad = hoofdstad;
        }

        public override bool Equals(Object obj)
        {
            var type = obj.GetType();
            if (type.Name == "Republiek")
            {
            
                Republiek r1 = (Republiek)obj;
                return this.Naam == r1.Naam;


            }
            else
                if (type.Name == "Monarchie")
            {
               
                Monarchie r2 = (Monarchie)obj;
                return this.Naam == r2.Naam;

            }
            else if (type.Name == "Land")
            {
              
                Land r3 = (Land)obj;
                return this.Naam == r3.Naam;
            }
            else
            {
                return false;
            }
                
        
           
 
        }
    }
}

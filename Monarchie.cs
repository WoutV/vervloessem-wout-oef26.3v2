﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vervloessem_Wout_oef26._3v2
{
    class Monarchie : Land
    {
        //private string _koning;
        public string Koning { get; set; }

        public Monarchie(string naam, string hoofdstad, string koning)
        {
            Naam = naam;
            Hoofdstad = hoofdstad;
            Koning = koning;
        }
        public override string ToString()
        {
            return base.ToString();
        }
    }
}

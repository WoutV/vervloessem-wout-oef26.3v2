﻿using System;
using System.Collections;
using System.Windows;
using System.Windows.Controls;

namespace Vervloessem_Wout_oef26._3v2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int iWelkeRegeringsvorm = 0;
        Object obj;
        ArrayList aLanden = new ArrayList();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void rbMonarchie_Checked(object sender, RoutedEventArgs e)
        {
            iWelkeRegeringsvorm = 0;
            obj = new Monarchie(tbLand.Text, tbHoofdstad.Text, tbStaatshoofd.Text);
        }

        private void rbRepubliek_Checked(object sender, RoutedEventArgs e)
        {
            iWelkeRegeringsvorm = 1;
            obj = new Republiek(tbLand.Text, tbHoofdstad.Text, tbStaatshoofd.Text);
        }

        private void rbOverige_Checked(object sender, RoutedEventArgs e)
        {
            iWelkeRegeringsvorm = 2;
            obj = new Land(tbLand.Text, tbHoofdstad.Text);
        }

        private void btnToevoegen_Click(object sender, RoutedEventArgs e)
        {
            tbOverzicht.Clear();
            if (aLanden == null )
            {
                aLanden.Add(obj);

            }
            else
            {
                if (aLanden.Count == 0)
                {
                    aLanden.Add(obj);
                   
                }
                else
                {
                    foreach (var item in aLanden)
                    {
                        if (item.Equals(obj))
                        {
                            MessageBox.Show("Deze data is al ingegeven");
                            break;
                        }
                        else
                        {
                            aLanden.Add(obj);
                            break;
                        }
                    }
                }
            }

            // opvullen textbox bovenaan
            TextFill(aLanden, tbOverzicht);
           
        }

     
        public void TextFill(ArrayList al, TextBox tb)
        {
            foreach (var item in al)
            {
                if (item is Monarchie)
                {
                    Monarchie landtemp = (Monarchie)item;
                    tb.Text += landtemp.Naam + "  " + "Koning(in) " + landtemp.Koning + ")";
                }
                else if (item is Republiek)
                {

                    Republiek landtemp1 = (Republiek)item;
                    tb.Text += landtemp1.Naam + "  " + "President " + landtemp1.President + ")";
                }
                else
                {

                    Land landtemp2 = (Land)item;
                    tb.Text += landtemp2.Naam + "  " + "Met als hoofdstad " + landtemp2.Hoofdstad + ")";
                }

                tb.Text += Environment.NewLine + "---------------------------------" + Environment.NewLine;

            }
        }
    }
   

}
